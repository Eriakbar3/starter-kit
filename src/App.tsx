import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter  } from 'react-router-dom'
import View from "./views";
function App() {
  return (
    <BrowserRouter>
      <View/>
    </BrowserRouter>
  );
}

export default App;
