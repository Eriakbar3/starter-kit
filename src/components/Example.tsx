import { useContext } from "react";
import { ExampleContext } from "../context/exampleContext";
const Example = ()=>{
  const example = useContext(ExampleContext)
  console.log(example);
  return (
    <>
    </>
  )
}
export default Example;
