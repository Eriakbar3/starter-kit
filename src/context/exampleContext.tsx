import React,{createContext  } from "react";

const ExampleContext = createContext({})

const ExampleProvider = (props:any)=>{
  const example = {
    text : "Hello Example Context"
  }
  return (
    <ExampleContext.Provider value={{example}}>
      {props.children}
    </ExampleContext.Provider>
  )
}
export {ExampleContext,ExampleProvider}
