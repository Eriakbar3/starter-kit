import React,{useContext} from "react";
import { ExampleProvider } from "../../context/exampleContext";
import  Example  from "../../components/Example";
 const Home = ()=>{
  return (
    <div>
      home
      <ExampleProvider>
          <Example/>
      </ExampleProvider>
    </div>
  )
}
export default Home;
